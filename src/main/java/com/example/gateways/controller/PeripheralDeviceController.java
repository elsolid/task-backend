package com.example.gateways.controller;

import com.example.gateways.entity.Gateway;
import com.example.gateways.entity.PeripheralDevice;
import com.example.gateways.repository.GatewayRepository;
import com.example.gateways.repository.PeripheralDeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/devices")
public class PeripheralDeviceController {

    @Autowired
    PeripheralDeviceRepository pRepository;

    @Autowired
    GatewayRepository gatewayRepository;

    @GetMapping
    public ResponseEntity<List<PeripheralDevice>> findAll() {
        return ResponseEntity.ok(pRepository.findAll());
    }

    @PostMapping("/{gatewayId}")
    public ResponseEntity<PeripheralDevice> create(@PathVariable String gatewayId,
                                                   @Valid @RequestBody PeripheralDevice device) {
        Gateway gateway = gatewayRepository.findById(gatewayId).get();
        device.setGateway(gateway);
        gateway.getPeripheralDevices().add(device);
        gatewayRepository.save(gateway);
        List<PeripheralDevice> devices = gateway.getPeripheralDevices();
        return ResponseEntity.ok(devices.get(devices.size() - 1));
    }

    @GetMapping("/{id}")
    public PeripheralDevice findById(@PathVariable Long id) {
        Optional<PeripheralDevice> device = pRepository.findById(id);
        if (!device.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return device.get();
    }

    @PutMapping("/{id}/{gatewayId}")
    public ResponseEntity<PeripheralDevice> update(@PathVariable Long id, @PathVariable String gatewayId,
                                                   @Valid @RequestBody PeripheralDevice PeripheralDevice) {
        Optional<PeripheralDevice> aux = pRepository.findById(id);
        if (!aux.isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        PeripheralDevice device = aux.get();
        Gateway gateway = gatewayRepository.findById(gatewayId).get();
        device.setVendor(PeripheralDevice.getVendor());
        device.setStatus(PeripheralDevice.getStatus());
        device.setGateway(gateway);
        return ResponseEntity.ok(pRepository.save(device));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        if (!pRepository.findById(id).isPresent()) {
            ResponseEntity.badRequest().build();
        }

        pRepository.deleteById(id);

        return ResponseEntity.ok().build();
    }

}
