package com.example.gateways.controller;

import com.example.gateways.entity.Gateway;
import com.example.gateways.repository.GatewayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;


@RestController
@RequestMapping("/api/gateways")
public class GatewayController {

    private final String IPv4_REGEX = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

    @Autowired
    GatewayRepository repository;

    @GetMapping
    public ResponseEntity<List<Gateway>> findAll() {
        return ResponseEntity.ok(repository.findAll());
    }

    @GetMapping("/{id}")
    public Gateway findById(@PathVariable String id) {
        Optional<Gateway> aux = repository.findById(id);
        if (!aux.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return aux.get();
    }

    @PostMapping
    public ResponseEntity<Gateway> create(@Valid @RequestBody Gateway gateway) {
        boolean isIpValid = Pattern.matches(IPv4_REGEX, gateway.getIpv4());
        if (isIpValid) {
            return ResponseEntity.ok(repository.save(gateway));
        } else {
            return ResponseEntity.badRequest().build();
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<Gateway> update(@PathVariable String id, @Valid @RequestBody Gateway gateway) {
        boolean isIpValid = Pattern.matches(IPv4_REGEX, gateway.getIpv4());

        Optional<Gateway> aux = repository.findById(id);
        if (!aux.isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        if (isIpValid) {
            Gateway gateway2 = aux.get();
            gateway2.setName(gateway.getName());
            gateway2.setIpv4(gateway.getIpv4());
            return ResponseEntity.ok(repository.save(gateway2));
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable String id) {
        if (!repository.findById(id).isPresent()) {
            ResponseEntity.badRequest().build();
        }
        repository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
